const DISCORD_WEBHOOKS = Deno.env.get("DISCORD_WEBHOOKS") || "";
const HOLIDAY_API_URL = "https://holidays-jp.github.io/api/v1/date.json";

function formatDate(date: Date) {
  const y = date.getFullYear().toString();
  const m = (date.getMonth() + 1).toString().padStart(2, "0");
  const d = date.getDate().toString().padStart(2, "0");
  return `${y}-${m}-${d}`;
}

function formatMessage(holidayName: string) {
  return `
  今日は祝日: ${holidayName}です。
  `;
}

const webhooks = DISCORD_WEBHOOKS.split(",");
if (webhooks.length === 0 && webhooks[0] === "")
  throw new Error("WebhookURLが一つも設定されていません。");

(async () => {
  const now = new Date();

  // 祝日リストを取得
  const res = await fetch(HOLIDAY_API_URL);
  const holidays: { [date: string]: string } = await res.json();

  console.debug(holidays);

  const today = formatDate(now);
  console.debug(today);

  // --- FOR DEBUG ---
  // holidays[today] = "テストの日";

  console.info("today:", today);

  const todayHolidayName = holidays[today];
  console.debug(todayHolidayName);

  if (todayHolidayName === undefined) {
    console.info("今日は祝日ではありません。");
    return;
  }

  console.info("今日は祝日:", todayHolidayName);

  // 今日が祝日の場合
  await Promise.all(
    webhooks.map((url) => {
      return fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          content: formatMessage(todayHolidayName),
        }),
      });
    })
  );
})();
